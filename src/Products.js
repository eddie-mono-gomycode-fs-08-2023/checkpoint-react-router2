import React from 'react';
import { useNavigate } from 'react-router';

const Products = () => {
  const navigate = useNavigate()
  const goods = [
    {
      id: 1,
      name: 'LG TV',
      price: 200,
      image: 'https://media.karousell.com/media/photos/products/2022/6/25/lg_oled_55_inch_smart_tv_2015__1656119559_d51cf2ed_progressive.jpg',
    },

    {
      id: 2,
      name: 'Nasco Gas Cooker',
      price: 200,
      image: 'https://noblesmartgh.com/wp-content/uploads/2021/12/60-x-60m-NASCO-4-BURNERS-GAS-COOKER-OVEN-GRIL-BLACK.jpg',
    },

    {
      id: 3,
      name: 'Samsung TV',
      price: 300,
      image: 'https://i0.wp.com/nextcashandcarry.com.ng/wp-content/uploads/2022/04/0001-6096547092_20210817_130050_0000.png?fit=1000%2C1000&ssl=1',
    },

    {
      id: 4,
      name: 'HP Laptop',
      price: 400,
      image: 'https://support.hp.com/doc-images/569/c06935891.png',
    },

    {
      id: 5,
      name: 'Phillips Home Theatre',
      price: 500,
      image: 'https://supersavings.lk/wp-content/uploads/2019/04/Philips-HTB5580_98-Home-Theater-System.jpg',
    },

    {
      id: 1, 
      name: 'BLING HANDWASH LIQUID SOAP', 
      volume: '5CL de volume',
      price: 1,
      image: 'https://melcom.com/media/catalog/product/cache/d0e1b0d5c74d14bfa9f7dd43ec52d082/1/0/103902a.jpg'
    },
  
    {id: 2, 
      name: 'Colgate', 
      volume: '5CL de volume',
      price: 2,
      image: 'https://images.squarespace-cdn.com/content/v1/5ac678af3c3a53092d3ae4e1/63133630-0388-42ab-b0f8-497b72213d0d/tri-colored-paste.png'
    },
  
    {id: 3, 
      name: 'Pacco Rabanne 1 million', 
      volume: '200ML de volume',
      price: 80,
      image: 'https://www.perfumeprice.co.uk/media/catalog/product/cache/89c597c5142d481043b34c4576f49c28/1/1/1162989-paco-rabanne-1-million-eau-de-toilette-spray-200ml.jpg'
    },
  
    {id: 4, 
      name: 'Hellmanns Mayonnaise', 
      volume: '2700CL de volume',
      price: 80,
      image: 'https://images.heb.com/is/image/HEBGrocery/000143819-1'
    },
  
    {id: 5, 
      name: 'Bonnet Rouge Lait', 
      volume: '2.5KG de volume',
      price: 80,
      image: 'https://www.ljastore.com/wp-content/uploads/2020/12/bonnet-rouge-25kg-scaled.jpg'
    },

    {
      id: 3,
    name: 'original-shirt', 
    color: 'Blue/Yellow', 
    price: 700, 
    image: 'https://m.media-amazon.com/images/I/61zLMxuIgEL.jpg'
    
    },

    {
      name: 'Pullover', 
    color: 'Blue/Yellow', 
    price: 300, 
    id: 3,
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2CfywEmgVrsmhncOCRPIazA18fF-4Sf982Q&usqp=CAU'
  },
  ];

  return (
    <>
    <h2 style={{backgroundColor: 'red'}}>Product List</h2>
    <div className='product'>

{goods.map(good => {
  
  return (
    <div className='product-card'>
    <ul>
    <img src={good.image} alt='product'></img>
    <div className='description'>
    <li key={good}>{good.name}</li>
    <li>${good.price}</li>
    <button onClick={() => navigate('/products/'+ good.name)}>See details</button>
  </div>
  </ul>
  </div>
  )
  
})}
    </div>
    </>
  );
}

export default Products;

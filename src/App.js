import {BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import ProductDetails from './ProductDetails';
import Navbar from './Navbar';
import Home from './Home';
import Products from './Products';
import Error from './Error';

function App() {
  return (
    <div className="App">
    <Navbar/>
    <div>

  <Routes>
    <Route path='/' element={<Home/>}/>
    <Route path='/products' element={<Products/>}/>
    <Route path='/products/:name' element={<ProductDetails/>}/>
    <Route path='/*' element={<Error/>}/>
  </Routes>
  

    </div>
    </div>
  );
}

export default App;
